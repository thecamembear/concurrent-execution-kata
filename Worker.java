import java.util.*;
import java.util.concurrent.*;

public class Worker {

    public ExecutedTasks execute(Collection<Runnable> actions, long timeoutMillis) throws InterruptedException {
        ExecutorService executorService = null;
        try {
            ExecutedTasks result = new ExecutedTasks();

            executorService = Executors.newFixedThreadPool(8);

            CountDownLatch masterLatch = new CountDownLatch(actions.size());

            for (Runnable action : actions) {
                executorService.execute(new RecordableRunnable(action, result, masterLatch, timeoutMillis, TimeUnit.MILLISECONDS));
            }

            boolean await = masterLatch.await(10, TimeUnit.SECONDS);
            if (await) {
                System.out.println("Completed tasks in time.");
            } else {
                System.out.println("Took too long.");
            }
            return result;
        }
        finally {
            if (executorService != null)
            {
                executorService.shutdown();
            }
        }
    }

    public static class RecordableRunnable implements Runnable {
        private final Runnable action;
        private final ExecutedTasks result;
        private final CountDownLatch masterLatch;
        private final long timeout;
        private final TimeUnit timeUnit;

        public RecordableRunnable(Runnable action, 
                ExecutedTasks result, 
                CountDownLatch masterLatch, 
                long timeout, 
                TimeUnit timeUnit) {
            this.action = action;
            this.result = result;
            this.masterLatch = masterLatch;
            this.timeout = timeout;
            this.timeUnit = timeUnit;
        }

        @Override
        public void run() {
            try {
                CompletableFuture<Void> voidCompletableFuture = CompletableFuture.runAsync(action);
                voidCompletableFuture.get(timeout, timeUnit);

                result.successful.add(action);
            }
            catch (TimeoutException e)
            {
                result.timedOut.add(action);
            }
            catch (Exception e) {
                result.failed.add(action);
            } finally {
                masterLatch.countDown();
            }
        }
    }

    public static class ExecutedTasks {
        public final List<Runnable> successful = new ArrayList<>();
        public final Set<Runnable> failed = new HashSet<>();
        public final Set<Runnable> timedOut = new HashSet<>();
    }
}
